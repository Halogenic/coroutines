This is just a little piece of experimental code written to see how the concept of coroutines could be implemented in Python.

Inspired by Unity3D's use of coroutines it has a similar interface to this and should work in much the same way.

I may use this in a future project such as a game engine or perhaps there will be some other uses for it outside of games.