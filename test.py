
from coroutine import Coroutine


TIME_DELTA = 0.0167


class WaitForSeconds(object):
    """
    Provides an iterator that only stops its iteration
    after the specified amount of time has elapsed.
    """

    def __init__(self, secs):
        self._secs = secs
        self._accumulated = 0.0

    def next(self):
        if self._accumulated < self._secs:
            # NOTE: the time delta that is added on would
            # be accessed from somewhere in the game
            # engine, for now we hardcode it as 0.0167 seconds
            # which is 1/60th of a second (60 fps).
            self._accumulated += TIME_DELTA
        else:
            raise StopIteration


class Simulation(object):

    def __init__(self):
        self.scripts = []
    
    def step(self):
        for script in self.scripts:
            script.update()


class Script(object):
    
    def __init__(self):
        self._coroutines = {}
    
    def update(self):
        self.__beginUpdate()

        self._localUpdate()

        self.__endUpdate()

    def startCoroutine(self, func):
        self._coroutines[func] = Coroutine(func)

    def pauseCoroutine(self, func):
        self._coroutines[func].pause()

    def resumeCoroutine(self, func):
        self._coroutines[func].resume()

    def stopCoroutine(self, func):
        del self._coroutines[func]

    def _localUpdate(self):
        pass

    def __beginUpdate(self):
        pass

    def __endUpdate(self):
        # update any coroutines that haven't finished yet

        # NOTE: not using iteritems() as this causes problems
        # when the dict changes in place, easier to just
        # create a copy of the items and iterate over those
        for func, coroutine in self._coroutines.items():
            try:
                coroutine.update()
            except StopIteration:
                del self._coroutines[func]


class CustomScript(Script):

    def __init__(self):
        super(CustomScript, self).__init__()

        self.startCoroutine(self._testCoroutine)
        self.startCoroutine(self._anotherCoroutine)

    def _localUpdate(self):
        pass

    def _testCoroutine(self):
        for i in xrange(10):
            print i
            yield

    def _anotherCoroutine(self):
        print "monkeys"

        yield WaitForSeconds(20)

        print "more monkeys"


sim = Simulation()
sim.scripts.append(CustomScript())

while True:
    sim.step()