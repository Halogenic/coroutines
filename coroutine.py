
import types


class Coroutine(object):
    """
    The class used for encapsulating the functionality
    of a coroutine. It can have its execution paused
    and subsequently resumed again.

    To use a coroutine in a method that allows you to
    quantize chunks of processing just call its update
    method to process each chunk in turn. The 'func' callback
    should be the function that determines what chunks
    to process at each point.

    It also provides the nesting of 'Coroutines' from its
    update method when calling the wrapped generator that
    was generated when first running the passed function.
    If this function itself yields an object with the
    next() interface then this will be treated as a coroutine.

    Usage:
    def coroutine_func():
        print 'something'

        yield

        print 'something else'

    coroutine = Coroutine(coroutine_func)
    coroutine.update()

    >>> something

    coroutine.update()

    >>> something else
    """
    
    def __init__(self, func):
        self._func = func

        # stores all generators in a stack-based data structure
        # allowing for nested coroutines to be used
        self._genStack = []

        self._paused = False

        self.__started = False

    def resume(self):
        self._paused = False

    def pause(self):
        self._paused = True

    def update(self):
        """
        Process the coroutines generator objects that are
        created from the original function callback.
        """

        if self._paused:
            return

        # only do these checks when running for the first time
        if not self.__started or not self._genStack:
            # the reason we do these checks here and not in __init__
            # is that when calling the func callback it will process
            # the first chunk of code up until the first yield, we 
            # only want processing to occur inside update()

            gen = self._func()

            if gen is None:
                return

            if not isinstance(gen, types.GeneratorType):
                raise Exception("Coroutine function must return a generator.")

            self._genStack.append(gen)

            self.__started = True

        gen = self._genStack[-1]

        try:
            retVal = gen.next()
        except StopIteration:
            self._genStack.pop()

            # coroutine has finished execution
            if not self._genStack:
                raise StopIteration
        else:
            if hasattr(retVal, "next") and callable(retVal.next):
                # the generator returned an object with
                # the next() interface and so this can
                # be used as a coroutine
                self._genStack.append(retVal)
